# Mysql connection and model class

> Simple lib with enhanced features to connect and manage your mysql database.

## Usage example

``` php
// Insert new user
$user_model
    ->setProp('username', 'theusername')
    ->setProp('sex', 'male')
    ->setProp('country', 'Indonesia')
    ->save()
    ->exec();
```

## Installation

```
# composer require phpid/db-core
```

## Get the PDO Connection object

Utilize the `Adapter` class provided in the library.

``` php
<?php
require __DIR__ . '/../vendor/autoload.php';

use Phpid\Db\Adapter;

$conf = [
    'db_name' => 'database_name',
    'user' => 'dbuser',
    'password' => 'dbpassword',
];
$pdo_conn = Adapter::getConnection($conf);
```

## Extending Model class

You have to provide `PDO` connection object from `Adapter::getConnection()` and `$options` Table options array as init params of the parent class.

``` php
<?php
require __DIR__ . '/../vendor/autoload.php';

use Phpid\Db\Adapter;
use Phpid\Db\MySQLModel;

class User extends MySQLModel
{
    public function __construct($options, $conn)
    {
        parent::__construct($conn);
        parent::setOptions($options);
    }
}
```
## Full Example

The file `tests/crud-test.php` will give you a more detailed description on utilizing the library.

In order to abstract your database model using the library you need to:

- [ ] Have a PDO Connection object
- [ ] Create your own model extending the model class from the library

``` php
<?php
require __DIR__ . '/../vendor/autoload.php';

// Extend the Model class
use Phpid\Db\MySQLModel;
use Phpid\Db\Adapter;

class City extends MySQLModel
{
    public function __construct($options, $conn)
    {
        parent::__construct($conn);
        parent::setOptions($options);
    }
}

// Get PDO Connection object
$conf = ['db_name' => 'testing_db', 'user' => 'testuser', 'password' => 'olalala'];
$pdo_conn = Adapter::getConnection($conf);

// Initialize the city model
$city = new City(['table'=>'cities'], $conn);
```

The `$city` will sits between your script and your database with predefined methods some of it is in the below examples.

``` php
// Insert a city
$jakut = $city
    ->setProp('name', 'Jakarta Utara')
    ->setProp('province', 'DKI Jakarta')
    ->setProp('reference', 'https://en.wikipedia.org/wiki/North_Jakarta')
    ->setProp('population', '1645312')
    ->create()
    ->exec();
var_dump($jakut);
```
Chain `setProp()` and `create()` for inserting a new row, close the chain with `exec()` to actually execute to your databae. `exec()` is use for `create()`, `save()` and `remove()` methods, while for `find()` and `get()` close the chain with `fetch()`.

``` php
// Insert another new city
$props = [
    'name' => 'Jakarta Timur',
    'province' => 'DKI Jakarta',
    'reference' => 'https://en.wikipedia.org/wiki/East_Jakarta',
    'population' => '2687027'
];
$jaktim = $city->setProps($props)->create()->exec();
var_dump($jaktim);
```

## Task Reminder

- [ ] Finish all base model methods
- [x] Create test files in `/tests`
- [ ] Finish the rest of the CRUD Method in the test file
- [ ] Annotate the classes
- [ ] Release initial
- [ ] Documentation
- [ ] Publish to packagist
