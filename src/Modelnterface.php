<?php
namespace Phpid\Db;

interface ModelInterface
{
    private static $conn;
    private $options;
    private $id;
    private $ids;
    private $props;
    private $collections;
    private $query;

    public function find();
    public function get($id);
    public function create();
    public function save();
    public function remove($id);
    public function fetch();
    public function exec();
    public function where($where);
    public function limit($limit);
    public function offset($offset);
    public function order($order);
    public function sort($sort);
    public function setOptions($options);
    public function setProp($attr,$value);
    public function setProps($props);
}
