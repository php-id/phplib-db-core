<?php
namespace Phpid\Db;

use PDOException;

class Adapter
{
    private static $conn = null;

    /**
     * Get PDO Database connection.
     *
     * It will throw a PDOException if not a PDO Connection.
     *
     * @param array $config Configuration array
     * @param string $db Database type, default mysql
     * @return mixed|PDO PDO Connection
     * @throws PDOException PDO Exception
     */

    public static function getConnection(array $config, string $db='mysql')
    {
        /**
         * $config should be an array with
         * - host (optional)
         * - user
         * - password
         * - db_name
         */

        $host = (isset($config['host'])) ? $config['host'] : 'localhost';
        $user = $config['user'];
        $pass = $config['password'];
        $name = $config['db_name'];

        if (self::$conn)
        {
            return self::$conn;
        }

        try
        {
            self::$conn = new \PDO("{$db}:host={$host};dbname={$name}", $user, $pass);
            self::$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return self::$conn;
        }
        catch (\PDOException $e)
        {
            throw new \PDOException($e->getMessage());
        }
    }
}
