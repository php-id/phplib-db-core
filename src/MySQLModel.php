<?php
namespace Phpid\Db;

class MySQLModel implements ModelInterface
{
    private static $conn = null;
    private static $statement = null;
    private $table = null;
    private $pkey = null;
    private $options = [];
    private $props = [];
    private $id = null;
    private $ids = [];
    private $collections = [];
    private $query = '';

    public function __construct(\PDO $conn)
    {
        self::$conn = $conn;
        $this->initDefaultOptions();
    }

    public function __destruct()
    {
        self::$conn = null;
    }

    public function find()
    {
        $this->query = "SELECT * FROM {$this->table}";
        return $this;
    }

    public function get($id)
    {
        $this->query = "SELECT * FROM {$this->table} WHERE `{$this->pkey}`=:id";
        $this->id = $id;
        return $this;
    }

    public function remove($id)
    {
        $this->query = "DELETE FROM {$this->table} WHERE `{$this->pkey}`=:id";
        $this->id = $id;
        return $this;
    }

    public function create()
    {
        $insert_params = '';
        $insert_keys = '';
        $keys = array_keys($this->props);
        $key_index = 0;

        foreach ($keys as $key)
        {
            if ($key_index > 0)
            {
                $insert_keys .= ',';
                $insert_params .= ',';
            }
            $insert_keys .= $key;
            $insert_params .= ":{$key}";
            $key_index++;
        }

        $this->query = "INSERT INTO {$this->table} ($insert_keys) VALUES ($insert_params)";
        return $this;
    }

    public function save()
    {
        $set_strings = '';
        $set_index = 0;

        foreach ($this->props as $key=>$val)
        {
            if ($set_index > 0)
            {
                $set_strings .= ',';
            }
            $set_strings .= "`{$key}`=:{$key}";
            $set_index++;
        }

        $this->props['id'] = $this->id;
        $this->query = "UPDATE {$this->table} SET {$set_strings} WHERE `{$this->pkey}`=:id";
        return $this;
    }

    public function fetch()
    {
        $this->verify();

        try
        {
            self::$statement = self::$conn->prepare($this->query);

            ($this->id) ?
                self::$statement->execute([ 'id' => $this->id ]) :
                self::$statement->execute();

            $this->collections = self::$statement->fetchAll(\PDO::FETCH_ASSOC);
            $this->populateIds();
            return $this->collections[0];
        }
        catch (\PDOException $e)
        {
            return $this->handleException('Error while executing your request '. $e->getMessage());
        }
    }

    public function exec()
    {
        $this->verify();

        try
        {
            $stmt = self::$conn->prepare($this->query);
            $stmt->execute($this->props);
            if (!$this->id)
            {
                $this->id = self::$conn->lastInsertId();
            }
            $this->populateCollections();
            return $this->collections;
        }
        catch (\PDOException $e)
        {
            return $this->handleException('Error while executing your request '. $e->getMessage());
        }
    }

    public function where($where)
    {
        $this->query .= " WHERE {$where}";
        return $this;
    }

    public function limit($limit)
    {
        $this->query .= " LIMIT {$limit}";
        return $this;
    }

    public function offset($offset)
    {
        $this->query .= " OFFSET {$offset}";
        return $this;
    }

    public function order($order)
    {
        $this->query .= " ORDER BY {$order}";
        return $this;
    }

    public function sort($sort)
    {
        $this->query .= " {$sort}";
        return $this;
    }

    public function setProp($attr, $value)
    {
        $this->props[$attr] = $value;
        return $this;
    }

    public function setProps($props)
    {
        $this->props = $props;
        return $this;
    }

    public function setOptions($options)
    {
        array_merge($this->options, $options);
        $this->table = $this->options['table'];
        $this->pkey = $this->options['pkey'];
        return $this;
    }

    private function populateCollections()
    {
        $this->collections = $this->get($this->id)->fetch();
    }

    private function handleException($msg)
    {
        echo $msg . ' ' . PHP_EOL;
        return false;
    }

    private function populateIds()
    {
        if (count($this->collections) > 1)
        {
            foreach ($this->collections as $coll)
            {
                $this->ids[] = $coll['id'];
            }
        }
    }

    private function initDefaultOptions()
    {
        $this->options = ['pkey' => 'id'];
    }

    /**
     * Verify table of properties prior to execution.
     *
     * @return bool true|false True if verified
     */
    private function verify()
    {
        if (!$this->table)
        {
            return $this->handleException("Define the table name while extending the model class.");
        }

        if (count($this->props) == 0)
        {
            return $this->handleException("Set props before creating or saving model.");
        }

        return true;
    }
}
