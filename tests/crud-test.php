<?php
/**
 * Test file for CRUD operations.
 */
require __DIR__ . '/../vendor/autoload.php';

// Extend the Model class
use Phpid\Db\MySQLModel;
use Phpid\Db\Adapter;

class City extends MySQLModel
{
    public function __construct($options, $conn)
    {
        parent::__construct($conn);
        parent::setOptions($options);
    }
}

function narate($msg)
{
    echo $msg . ' ' . PHP_EOL;
}

// Get the PDO Connection
narate('Getting the PDO Connection object.');
$conf = ['db_name' => 'testing_db', 'user' => 'testuser', 'password' => 'olalala'];
$pdo_conn = Adapter::getConnection($conf);
narate('Got the PDO Connection object.');

// Initialize the City object
$city = new City(['table' => 'cities'], $conn);
// Insert Jakarta Utara
narate('Inserting new city to the database.');
$jakut = $city
    ->setProp('name', 'Jakarta Utara')
    ->setProp('province', 'DKI Jakarta')
    ->setProp('reference', 'https://en.wikipedia.org/wiki/North_Jakarta')
    ->setProp('population', '1645312')
    ->create()
    ->exec();
var_dump($jakut);
narate('New city inserted.');
// Insert Jakarta Timur
narate('Inserting another new city to the database.');
$props = [
    'name' => 'Jakarta Timur',
    'province' => 'DKI Jakarta',
    'reference' => 'https://en.wikipedia.org/wiki/East_Jakarta',
    'population' => '2687027'
];
$jaktim = $city->setProps($props)->create()->exec();
var_dump($jaktim);
narate('New city inserted.');

// Select operations
narate('Get cities from the database.');
$cities = $city->find()->fetch();
var_dump($cities);
narate('Got cities from the database.');

// Select with condition
narate('Get cities from the database with where condition.');
$cities_where = $city->find()->where("`name`='Jakarta Utara'")->fetch();
var_dump($cities_where);
narate('Got cities from the database.');



